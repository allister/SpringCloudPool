# SpringCloudPool

1. sc_eurekaserver ====== 注册中心（服务的注册与发现（Eureka）） 

2. sc_service-hi   ======服务提供者

3. sc_service-ribbon ==== 服务消费者（rest+ribbon）& 断路器（Hystrix）

4. sc_service-feign ==== 服务消费者 （Feign）

5. sc_service-zuul  ==== 路由网关(zuul)

